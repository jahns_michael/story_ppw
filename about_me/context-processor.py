from datetime import datetime

def date_time(request):
    myDate = datetime.now()
    formatedDate = myDate.strftime("%Y-%m-%d %H:%M:%S") 
    return {
        'date': formatedDate
    }