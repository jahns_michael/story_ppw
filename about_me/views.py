from django.shortcuts import render
from datetime import datetime

# Create your views here.
def index(request):
    return render(request, 'index.html')

def page1(request):
    return render(request, 'page1.html')