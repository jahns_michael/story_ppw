from django.urls import include, path
from .views import index, page1

urlpatterns = [
    path('', index, name='index'),
    path('page1/', page1, name='page1'),
]

